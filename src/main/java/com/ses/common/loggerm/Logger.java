package com.ses.common.loggerm;

import java.io.File;

import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;


/**
 * The Wrapping class of logback
 * 
 * <p> This class is provide a writing log to console, files.
 * <pre>
 * 	Logger.init(LogLevel.DEBUG);
 *	Logger.debug("class name", "log message"); 
 * </pre>
 * 
 * @author Eungsuk Shon
 * @since 1.0
 * @see com.ses.common.loggerm.LogLevel
 *
 */
public class Logger {
  private static Logger mInstance_ = null;

  private static final String KEY_LOG_DIR = "LOG_DIR";
  private static final String KEY_LOG_FILE_NAME = "LOG_FILE_NAME";
  private static final String KEY_LOG_FILE_EXTENSION = "LOG_FILE_EXTENSION";
  
  public static void init(LogLevel debug) {
	  mInstance_ = new Logger();
      mInstance_.setLogLevel_(debug);
      mInstance_.mWriteToFile_ = true;      
  }  

  private static void printWarning_() {
    System.out.println("Logger should be initialized before use it!!!");
  }
  
  private static void print_(String tag, String msg) {
	  System.out.println(tag + " : " + msg);
  }
    
  public static void debug(String tag, String msg) {
    if (mInstance_ != null) {
      mInstance_.debug_(tag + " " + msg);
    } else {
    	print_(tag, msg);
    }
  }

  public static void info(String tag, String msg) {
    if (mInstance_ != null) {
      mInstance_.info_(tag + " " + msg);
    } else {
    	print_(tag, msg);
    }
  }

  public static void warn(String tag, String msg) {
    if (mInstance_ != null) {
      mInstance_.warn_(tag + " " + msg);
    } else {
    	print_(tag, msg);
    }
  }
  
  public static void error(String tag, String msg) {
	  error(tag, new Error(msg));
  }

  public static void error(String tag, Throwable t) {
    if (mInstance_ != null) {
      mInstance_.error_(tag, t);
    } else {
    	print_(tag, t.getMessage());
    }
  }

  public static void enable(boolean enabled) {
    if (mInstance_ != null) {
      mInstance_.mWriteToFile_ = enabled;
    } else {
      printWarning_();
    }
  }

  public static void enableConsole(boolean enabled) {
    if (mInstance_ != null) {
      mInstance_.mPrintToConsole_ = enabled;
    } else {
      printWarning_();
    }
  }

  public static void setLevel(LogLevel level) {
    if (mInstance_ != null) {
      mInstance_.setLogLevel_(level);
    } else {
      printWarning_();
    }
  }

  public static File getLogFile() {
    if (mInstance_ != null) {
      return mInstance_.getTargetDirectoryFile_();
    } else {
      printWarning_();
    }
    return null;
  }

  private org.slf4j.Logger mFileLogger_ = null;
  private org.slf4j.Logger mConsoleLogger_ = null;
  private LogLevel mLogLevel_ = LogLevel.ERROR;
  private boolean mWriteToFile_ = false;
  private boolean mPrintToConsole_ = true;

  private Logger() {
    ILoggerFactory factory = LoggerFactory.getILoggerFactory();

    mFileLogger_ = factory.getLogger("file");
    mConsoleLogger_ = factory.getLogger("con");
  }

  private File getTargetDirectoryFile_() {
    File ret = null;
    String directory = System.getProperty(KEY_LOG_DIR);
    String filename = System.getProperty(KEY_LOG_FILE_NAME);
    String extension = System.getProperty(KEY_LOG_FILE_EXTENSION);

    if (directory != null && filename != null && extension != null) {
      ret = new File(directory, filename + "." + extension);
    }

    return ret;
  }

  private void debug_(String msg) {
    if (mLogLevel_.compareTo(LogLevel.DEBUG) >= 0) {
      if (mWriteToFile_) {
        mFileLogger_.debug(msg);
      }
      if (mPrintToConsole_) {
        mConsoleLogger_.debug(msg);
      }
    }
  }

  private void info_(String msg) {
    if (mLogLevel_.compareTo(LogLevel.INFO) >= 0) {
      if (mWriteToFile_) {
        mFileLogger_.info(msg);
      }
      if (mPrintToConsole_) {
        mConsoleLogger_.info(msg);
      }
    }
  }

  private void warn_(String msg) {
    if (mLogLevel_.compareTo(LogLevel.WARNING) >= 0) {
      if (mWriteToFile_) {
        mFileLogger_.warn(msg);
      }
      if (mPrintToConsole_) {
        mConsoleLogger_.warn(msg);
      }
    }
  }

  private void error_(String msg, Throwable t) {
    if (mLogLevel_.compareTo(LogLevel.ERROR) >= 0) {
      if (mWriteToFile_) {
    	  mFileLogger_.error(msg, t);
      }
      if (mPrintToConsole_) {
        mConsoleLogger_.error(msg, t);
      }
    }
  }

  private void setLogLevel_(LogLevel logLevel) {
    mLogLevel_ = logLevel;
  }
}
