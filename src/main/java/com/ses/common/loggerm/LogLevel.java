package com.ses.common.loggerm;

/**
 * The enumeration that defined log level.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 * @see com.ses.common.loggerm.Logger
 * 
 */
public enum LogLevel {
	ERROR, WARNING, INFO, DEBUG;
}
