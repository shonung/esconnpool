package com.ses.common.configm;

/**
 * Interface of Config Manager
 * 
 * <p> Interface for abstraction Config Manager.
 * If you want to add a Config Manager's implementation, 
 * please implements this interface class. 
 * 
 * @author Eungsuk Shon
 * @since 1.0
 */
public interface IConfigManager {
	public String get(String key);	
}
