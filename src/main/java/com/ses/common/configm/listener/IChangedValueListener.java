package com.ses.common.configm.listener;

/**
 * Interface for listening event that changed value in configuration files.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 */

//TODO: implements this listener in Config Manager for applying the changing value. 
public interface IChangedValueListener {
	public void OnChangedValue();
}
