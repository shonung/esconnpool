package com.ses.common.configm;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ses.common.configm.model.JsonSettingDefine;
import com.ses.common.configm.model.JsonSettingValue;
import com.ses.common.loggerm.Logger;

/**
 * Config Manager's implementation of JSON.
 * 
 * <p> This class provides the reading *.json.
 * Now reads a configuration that the pre-defined in JsonSettingDefine.
 * If you want to all value in *.json, you will modify readJsonAndSetValue() method.  
 * 
 * @author Eungsuk Shon
 * @since 1.0
 */
public class JsonConfigManager implements IConfigManager {
	private static String TAG = JsonConfigManager.class.getSimpleName();
	private static final JsonConfigManager instance = new JsonConfigManager();	
	private String fileName = null;
	private ObjectMapper mapper = new ObjectMapper();
	
	public static JsonConfigManager getInstance() {
		return instance;
	}
	
	public JsonConfigManager init(String fileName) {
		if (this.fileName == null && fileName != null && instance != null) {
			instance.setFileName(fileName);
			readJsonAndSetValue();
		}
		return instance;
	}
		
	// don't guarantee not-null	
	public String get(String key) {
		if (key != null) {
			return System.getProperty(key);
		}
		return null;
	}
	
	private void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	private void readJsonAndSetValue() {
		 try {
			 JsonSettingValue value = mapper.readValue(new File(fileName), JsonSettingValue.class);			 
			 set(JsonSettingDefine.POOL_SIZE.getDefine(), Integer.toString(value.poolSize));
			 set(JsonSettingDefine.RETRY_INTERVAL.getDefine(), Integer.toString(value.retryInterval));
			 set(JsonSettingDefine.TIMEOUT.getDefine(), Integer.toString(value.timeout));
		 } catch(Exception e) {
			 Logger.error(TAG, e);
		 }
	}	
	
	private void set(String key, String value) {
		if (key != null && value != null) {
			synchronized (this) {
				System.setProperty(key, value);
			}
		}
	}
}
