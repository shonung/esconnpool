package com.ses.common.configm;

/**
 * Factory Class for making Config Manager
 * 
 * <p> This class can make various Config Manager.
 * If you want to make another Config Manager (i.e. XML Config Manager), 
 * define enumeration and implement getConfigManager method. 
 * 
 * @author Eungsuk Shon
 * @since 1.0
 */
public enum ConfigManagerFactory {
	JSON("resources/settings.json") {
		public IConfigManager getConfigManager() { return JsonConfigManager.getInstance().init(JSON.getName()); }		
	};
	
	public abstract IConfigManager getConfigManager();
	
	final private String fileName;    
    public String getName() {
        return fileName;
    }
 
    private ConfigManagerFactory(String fileName){
        this.fileName = fileName;
    }
}
