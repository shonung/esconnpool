package com.ses.common.configm.model;

/**
 * The enumeration that defined in json files.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 */
public enum JsonSettingDefine {
	POOL_SIZE("poolSize"),
	RETRY_INTERVAL("retryInterval"),
	TIMEOUT("timeout");
	
	private String settingString;
	
	JsonSettingDefine(String settingString) {
		this.settingString = settingString;
	}
	
	public String getDefine() {
		return this.settingString;
	}
}
