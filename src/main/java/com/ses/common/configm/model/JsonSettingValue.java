package com.ses.common.configm.model;

/**
 * Model class for jackson's ObjectMapper.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 * @see com.fasterxml.jackson.databind.ObjectMapper
 *
 */
public class JsonSettingValue {
	public int poolSize; // 1 ~
	public int retryInterval; // milli sec
	public int timeout; // milli sec
}
