package com.ses.core.connpool.handler;

import com.ses.common.configm.model.JsonSettingDefine;
import com.ses.common.loggerm.Logger;
import com.ses.core.connpool.ConnFactory;
import com.ses.core.connpool.connection.Conn;
import com.ses.core.connpool.exception.FailToCloseConnException;
import com.ses.core.connpool.model.ConnObj;
import com.ses.core.connpool.queue.CloseCheckListener;
import com.ses.core.connpool.queue.ConnPoolQueue;

/**
 * Connection Pool Handler for managing queue in Connection Pool.
 * 
 * <p> Connection Pool Handler manage queue in Connection Pool.
 * Connection Pool periodically create connection and insert to queue. 
 * 
 * @author Eungsuk Shon
 * @since 1.0
 */
public class ConnPoolHandler implements Runnable {
	private static String TAG = ConnPoolHandler.class.getSimpleName();
	@SuppressWarnings("rawtypes")
	private ConnFactory cf;
	private boolean stopped = false;
	private ConnCloseHander closeHandler = new ConnCloseHander();
	
	@SuppressWarnings("rawtypes")
	public ConnPoolHandler(ConnFactory cf) {
		this.cf = cf;
	}
	
	@Override
	public void run() {
		while(!stopped) {
			try {
				if (!isFull()) {
					createAndPushConn();
				}
				Thread.sleep(1000);
			} catch(Exception e) {
				Logger.error(TAG, e);
			}
		}		
	}
	
	public void stop() {
		stopped = true;
		Logger.info(TAG, "ConnPoolHandler is terminated");
		Thread.currentThread().interrupt();		
	}
	
	private boolean isFull() {
		return (ConnPoolQueue.getCurrentSize() < Integer.parseInt( System.getProperty(JsonSettingDefine.POOL_SIZE.getDefine())))
				? false : true;
	}
	
	private void createAndPushConn() {
		Conn con = cf.getInstatance();	
		con.registerConnCloser(closeHandler);
		ConnPoolQueue.add(new ConnObj(con));		
	}
	
	
	public class ConnCloseHander implements CloseCheckListener {
		@Override
		public void OnCloseConn(Conn con) throws FailToCloseConnException {
			ConnPoolQueue.remove(con);
		}		
	}
}
