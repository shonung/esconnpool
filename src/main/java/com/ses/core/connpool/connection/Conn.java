package com.ses.core.connpool.connection;

import com.ses.common.loggerm.Logger;
import com.ses.core.connpool.exception.FailToCloseConnException;
import com.ses.core.connpool.queue.CloseCheckListener;

/**
 * Connection class
 * 
 * <p> The Conn class define common function for connection.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 *
 */
public abstract class Conn {
	private static String TAG = Conn.class.getSimpleName();
	private CloseCheckListener connCloser;

	public Conn(){
		createResource();
		generate();
	}

	public void registerConnCloser(CloseCheckListener connCloser) {
		this.connCloser = connCloser;
	}

	public void close() throws FailToCloseConnException {
		try {
			connCloser.OnCloseConn(this);
		} catch (Exception e) {
			Logger.error(TAG, e);
			throw new FailToCloseConnException(e);
		}
	}

	public abstract void createResource();
	public abstract void generate();
}
