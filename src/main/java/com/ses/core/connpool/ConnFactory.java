package com.ses.core.connpool;

import com.ses.common.loggerm.Logger;
import com.ses.core.connpool.connection.Conn;

/**
 * Factory Class for making connection.
 * 
 * <p> This class can make various Connection from user.
 * This class will be used in Connection Pool.
 * If you wants to use the Connection Pool, you must create instance of this class and input Connection Pool.
 * And you must input class (that implements Conn class) name in the parameter T.
 * 
 * <pre>
 * 	ConnFactory<CustomizedConnImpl> cf = new ConnFactory<>(class that you implement.class);
 * </pre> 
 * 
 * @author Eungsuk Shon
 * @since 1.0
 * @param <T>
 */
public class ConnFactory<T extends Conn> {
	private static String TAG = ConnFactory.class.getSimpleName();
	
	private Class<T> clazz;
	
	public ConnFactory(Class<T> cls) {
		clazz = cls;
	}
	
	public T getInstatance() {
		try {
			return clazz.newInstance();
		} catch (Exception e) {
			Logger.error(TAG, e);
			return null;
		}
	}
}
