package com.ses.core.connpool.exception;

/**
 * Exception for notifying that failed to get connection pool.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 *
 */
public class FailToGetConnException extends Exception {
	private static final long serialVersionUID = 6163072287687060506L;

	public FailToGetConnException(String string) {
		super(string);
	}

	public FailToGetConnException(Exception e) {
		super(e);
	}
}
