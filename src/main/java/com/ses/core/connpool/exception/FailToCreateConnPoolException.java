package com.ses.core.connpool.exception;

/**
 * Exception for notifying that failed to create connection pool.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 *
 */
public class FailToCreateConnPoolException extends Exception {
	private static final long serialVersionUID = 7829295937281681849L;

	public FailToCreateConnPoolException(String string) {
		super(string);
	}

	public FailToCreateConnPoolException(Exception e) {
		super(e);
	}
}
