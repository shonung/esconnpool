package com.ses.core.connpool.exception;

/**
 * Exception for notifying that failed to return connection pool.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 *
 */
public class FailToReturnConnExceptipon extends Exception {
	private static final long serialVersionUID = -4625116043858505975L;
	
	public FailToReturnConnExceptipon(String string) {
		super(string);
	}

	public FailToReturnConnExceptipon(Exception e) {
		super(e);
	}

}
