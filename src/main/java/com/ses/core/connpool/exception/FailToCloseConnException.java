package com.ses.core.connpool.exception;

/**
 * Exception for notifying that failed to close connection.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 *
 */
public class FailToCloseConnException extends Exception {
	private static final long serialVersionUID = -6652425791008593179L;
	
	public FailToCloseConnException(String string) {
		super(string);
	}

	public FailToCloseConnException(Exception e) {
		super(e);
	}
}
