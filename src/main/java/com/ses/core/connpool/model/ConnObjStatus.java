package com.ses.core.connpool.model;

/** 
 * Connection Object Status
 * 
 * <p> This enumeration defines status of Connection Object.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 *
 */
public enum ConnObjStatus {
	WAITING, BORROWED;
}
