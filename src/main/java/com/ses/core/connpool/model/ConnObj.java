package com.ses.core.connpool.model;

import com.ses.core.connpool.connection.Conn;

/**
 * Connection Object has connection and connection status.
 * 
 *  <p> Connection Object has connection and connection status whether waiting or borrowed.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 *
 */
public class ConnObj {
	private Conn con;
	private ConnObjStatus status;
	
	public ConnObj(Conn con) {
		this.con = con;
		this.status = ConnObjStatus.WAITING;
	}
	
	public Conn getCon() {
		return con;
	}
	
	public ConnObjStatus getConnObjStatus() {
		return status;
	}
	
	public void setCon(Conn con) {
		this.con = con;
	}
	
	public void setConnObjStatus(ConnObjStatus status) {
		this.status = status;
	}
	
}
