package com.ses.core.connpool;

import com.ses.core.connpool.exception.FailToCreateConnPoolException;


/**
 * Connection Pool Factory for making Connection Pool.
 * 
 * <p> This class can make various connection pool.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 *
 */
public class ConnPoolFactory {	
	@SuppressWarnings("unchecked")
	public static ConnPool getConnPool(@SuppressWarnings("rawtypes") ConnFactory connFactory) throws FailToCreateConnPoolException { return new ConnPool(connFactory); }
}
