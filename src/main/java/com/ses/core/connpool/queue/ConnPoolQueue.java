package com.ses.core.connpool.queue;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import com.ses.common.loggerm.Logger;
import com.ses.core.connpool.connection.Conn;
import com.ses.core.connpool.exception.FailToCloseConnException;
import com.ses.core.connpool.exception.FailToGetConnException;
import com.ses.core.connpool.model.ConnObj;
import com.ses.core.connpool.model.ConnObjStatus;

/**
 * Connection Pool Queue for storing and managing connection instance.
 * 
 * <p> Connection Pool Queue storage and manage connection instance.
 * Connection Pool Queue provides Connection Object through get() method.
 * The get() method has operated through separated thread.
 * The queue size is decided by configuration. You can change pool size by changing value in configuration.json file.
 * If there is no available connection, Connection Pool retry to get the available connection during timeout.
 * The retry count and timeout also are decided by configuration. You can change retry count, timeout by changing value in configuration.json file. 
 * @author Eungsuk Shon
 * @since 1.0
 *
 */
public class ConnPoolQueue {
	private static String TAG = ConnPoolQueue.class.getSimpleName();
	private static List<ConnObj> queue;
	private static ExecutorService executor = Executors.newCachedThreadPool();
	private static AtomicInteger totalCnt = new AtomicInteger(0);
	private static AtomicInteger borrowedCnt = new AtomicInteger(0);
	private static int poolMaxSize;	
	private static int retryInterval;
	private static int retryCnt;
	

	public ConnPoolQueue(int poolSize, int retryInterval, int timeout) {
		queue = new Vector<ConnObj>(poolSize);
		ConnPoolQueue.poolMaxSize = poolSize;
		ConnPoolQueue.retryCnt = (timeout/retryInterval);
		ConnPoolQueue.retryInterval = retryInterval;		
		totalCnt.set(0);
		borrowedCnt.set(0);
	}

	public static int getCurrentSize() {
		return queue.size();
	}

	public static void add(ConnObj co) {
		try {
			queue.add(co);
			totalCnt.set(queue.size());
			Logger.debug(TAG, "ConnObj added : " + co);
		} catch (Exception e) {
			Logger.error(TAG, e);
		}
	}

	public static void remove(Conn con) throws FailToCloseConnException {
		try {
			for (int i = 0; i < queue.size(); i++) {
				if (queue.get(i).getCon().hashCode() == con.hashCode()) {
					queue.remove(i);
					borrowedCnt.getAndDecrement();
					totalCnt.set(queue.size());
					return;
				}
			}
		} catch (Exception e) {
			Logger.error(TAG, e);
			throw new FailToCloseConnException(e);
		}
	}
	
	public static Conn get() throws FailToGetConnException {
		try {
			return executor.submit(new Callable<Conn>() {
				@Override
				public Conn call() throws Exception {						
					for (int i = 0; i < retryCnt; i++) {
						if (totalCnt.get() > borrowedCnt.get()) {
							for (ConnObj connObj : queue) {
								if (connObj.getConnObjStatus().compareTo(ConnObjStatus.WAITING) == 0) {
									connObj.setConnObjStatus(ConnObjStatus.BORROWED);
									borrowedCnt.getAndIncrement();
									return connObj.getCon();
								}
							}
						}
						Logger.info(TAG, "Already all connection was borrowed by some user. Retry to get..");
						Thread.sleep(retryInterval);
					}
					Logger.info(TAG, "Timeout. Try it again.");
					throw new FailToGetConnException("Timeout. Try it again.");
				}
			}).get();
		} catch (Exception e) {
			Logger.error(TAG, e);
			throw new FailToGetConnException(e);
		} finally {			
		}
		
	}

	public static void returnCon(Conn con) {
		try {
			ConnObj connObj = compareAndReturnConnObj(con);
			if (connObj != null) {
				connObj.setConnObjStatus(ConnObjStatus.WAITING);
				borrowedCnt.getAndDecrement();
			} else {
				Logger.error(TAG, "There is no matched connection");
			}
		} catch (Exception e) {
			Logger.error(TAG, e);
		}
	}

	public static ConnPoolQueueInfo getInfo() {
		return new ConnPoolQueueInfo(totalCnt.get(), borrowedCnt.get(), poolMaxSize);
	}

	private static ConnObj compareAndReturnConnObj(Conn con) {
		try {
			for (ConnObj connObj : queue) {
				if (connObj.getCon().hashCode() == con.hashCode()
						&& connObj.getConnObjStatus().compareTo(ConnObjStatus.BORROWED) == 0) {
					return connObj;
				}
			}

		} catch (Exception e) {
			Logger.error(TAG, e);
		}

		return null;
	}

}
