package com.ses.core.connpool.queue;

import com.ses.core.connpool.connection.Conn;
import com.ses.core.connpool.exception.FailToCloseConnException;

/**
 * Interface for listening event that close connection by user. 
 * 
 * @author Eungsuk Shon
 * @since 1.0
 *
 */
public interface CloseCheckListener {
	public void OnCloseConn(Conn con) throws FailToCloseConnException;
}
