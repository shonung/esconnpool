package com.ses.core.connpool.queue;

import com.ses.common.loggerm.Logger;

/**
 * The information of Connection Pool Queue
 * 
 * @author Eungsuk Shon
 * @since 1.0
 *
 */
public class ConnPoolQueueInfo {
	private static String TAG = ConnPoolQueueInfo.class.getSimpleName();
	private int totalCnt;
	private int borrowedCnt;
	private int poolMaxSize;
	
	ConnPoolQueueInfo(int totalCnt, int borrowedCnt, int poolMaxSize) {
		this.totalCnt = totalCnt;
		this.borrowedCnt = borrowedCnt;
		this.poolMaxSize = poolMaxSize;
	}
	
	public int getTotalCnt() {
		return totalCnt;
	}
	
	public int getBorrowedCnt() {
		return borrowedCnt;
	}
	
	public void setTotalCnt(int totalCnt) {
		this.totalCnt = totalCnt;
	}
	
	public void setBorrowedCnt(int borrowedCnt) {
		this.borrowedCnt = borrowedCnt;
	}
	
	public void showConnPoolStatus() {
		Logger.info(TAG, "----------------------");
		Logger.info(TAG, "poolMaxSize : " + poolMaxSize);
		Logger.info(TAG, "totalCnt : " + totalCnt);
		Logger.info(TAG, "borrowedCnt : " + borrowedCnt);		
		Logger.info(TAG, "----------------------");
	}
}
