package com.ses.core.connpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.ses.app.CustomizedConnImpl;
import com.ses.common.configm.ConfigManagerFactory;
import com.ses.common.configm.IConfigManager;
import com.ses.common.configm.model.JsonSettingDefine;
import com.ses.common.loggerm.Logger;
import com.ses.core.connpool.connection.Conn;
import com.ses.core.connpool.exception.FailToCreateConnPoolException;
import com.ses.core.connpool.exception.FailToGetConnException;
import com.ses.core.connpool.exception.FailToReturnConnExceptipon;
import com.ses.core.connpool.handler.ConnPoolHandler;
import com.ses.core.connpool.queue.ConnPoolQueue;
import com.ses.core.connpool.queue.ConnPoolQueueInfo;

/**
 * The Connection Pool for managing connection resource.
 * 
 * <p> The Connection Pool provide the pre-created connection.
 * The Connection Pool has a many connection in Connection Pool Queue.
 * The queue size is decided by configuration. You can change pool size by changing value in configuration.json file.
 * If there is no available connection, Connection Pool retry to get the available connection during timeout.
 * The retry count and timeout also are decided by configuration. You can change retry count, timeout by changing value in configuration.json file.
 * You must return connection after use done, because connections in Connection Pool are limit.
 * And you can close connection after use done. If you close connection, Connection Pool make a new connection.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 * 
 */
public class ConnPool {
	private static String TAG = ConnPool.class.getSimpleName();
	private ConnFactory<Conn> connFactory;
	private ConnPoolHandler connPoolHandler;
	@SuppressWarnings("unused")
	private ConnPoolQueue connPoolQueue;
	private IConfigManager cm = ConfigManagerFactory.JSON.getConfigManager();
	private ExecutorService executorService = Executors.newSingleThreadExecutor();
	
	ConnPool(ConnFactory<Conn> connFactory) throws FailToCreateConnPoolException {
		this.connFactory = connFactory;
		init();
	}
	
	/**
	 * Stop the Connection Pool.
	 * The Connection Pool Handler will be stopped and the all resources in Connection Pool are released.
	 * 
	 * <pre>
	 * 	ConnFactory<CustomizedConnImpl> cf = new ConnFactory<>(CustomizedConnImpl.class);
	 *  ConnPool cp;
	 * 	try {
			cp = ConnPoolFactory.getConnPool(cf);
		} catch (FailToCreateConnPoolException e) {
			Logger.error(TAG, e);
		}
		cp.stop();
	 * </pre>
	 * 
	 * @author Eungsuk Shon
	 * @since 1.0
	 *
	 */
	public void stop() {
		connPoolHandler.stop();
		executorService.shutdown();
	}
	
	/**
	 * Get the Connection Pool.	  
	 * 
	 * <pre>
	 * 	ConnFactory<CustomizedConnImpl> cf = new ConnFactory<>(CustomizedConnImpl.class);
	 *  ConnPool cp;
	 * 	try {
			cp = ConnPoolFactory.getConnPool(cf);
		} catch (FailToCreateConnPoolException e) {
			Logger.error(TAG, e);
		}		
	 * </pre>
	 * 
	 * @author Eungsuk Shon
	 * @since 1.0
	 * @throws FailToGetConnException
	 *
	 */
	public Conn getConn() throws FailToGetConnException {
		return ConnPoolQueue.get();
	}
	
	/**
	 * Return the Connection Pool. 
	 * 
	 * <pre>
	 * 	ConnFactory<CustomizedConnImpl> cf = new ConnFactory<>(CustomizedConnImpl.class);
	 *  ConnPool cp;
	 * 	try {
			cp = ConnPoolFactory.getConnPool(cf);
		} catch (FailToCreateConnPoolException e) {
			Logger.error(TAG, e);
		}		
	 * </pre>
	 * 
	 * @author Eungsuk Shon
	 * @since 1.0
	 * @throws FailToReturnConnExceptipon
	 *
	 */
	public void returnConn(Conn con) throws FailToReturnConnExceptipon {
		ConnPoolQueue.returnCon(con);
	}
	
	/**
	 * Get the Connection Pool's information. 
	 * 
	 * <pre>
	 * 	ConnFactory<CustomizedConnImpl> cf = new ConnFactory<>(CustomizedConnImpl.class);
	 *  ConnPool cp;
	 * 	try {
			cp = ConnPoolFactory.getConnPool(cf);
		} catch (FailToCreateConnPoolException e) {
			Logger.error(TAG, e);
		}		
		
		ConnPoolQueueInfo info = cp.getConnPoolInfo();
		info.showConnPoolStatus();
	 * </pre>
	 * 
	 * @author Eungsuk Shon
	 * @since 1.0	 
	 *
	 */
	public ConnPoolQueueInfo getConnPoolInfo() {
		return ConnPoolQueue.getInfo();
	}
	
	@Override
	protected void finalize() throws Throwable {		
		connPoolHandler.stop();
		executorService.shutdown();
	}
	
		
	private void init() throws FailToCreateConnPoolException {		
		try {
			connPoolQueue = new ConnPoolQueue(
					Integer.parseInt(cm.get(JsonSettingDefine.POOL_SIZE.getDefine())),
					Integer.parseInt(cm.get(JsonSettingDefine.RETRY_INTERVAL.getDefine())),
					Integer.parseInt(cm.get(JsonSettingDefine.TIMEOUT.getDefine())));
			connPoolHandler = new ConnPoolHandler(connFactory);	
			executorService.execute(connPoolHandler);
		} catch (Exception e) {
			Logger.error(TAG, e);
			throw new FailToCreateConnPoolException(e);
		}
	}
	
}
