
package com.ses.app;

import com.ses.common.loggerm.LogLevel;
import com.ses.common.loggerm.Logger;

/**
 * Main Class
 * 
 * <p> Just initiates Logger.
 * 
 * 
 * @author Eungsuk Shon
 * @since 1.0
 */
public class App {	
	public static void main(String[] args) {
		Logger.init(LogLevel.DEBUG);
	}
}
