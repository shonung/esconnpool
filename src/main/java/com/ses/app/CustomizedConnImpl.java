package com.ses.app;

import com.ses.core.connpool.connection.Conn;

/**
 * Customized Connection Class by user
 * 
 * <p> Just example for reference.
 * 
 * @author Eungsuk Shon
 * @since 1.0
 *
 */
public class CustomizedConnImpl extends Conn {	
	public CustomizedConnImpl(){
		super();		
	}

	@Override
	public void createResource() {		
	}

	@Override
	public void generate() {		
	}	
}
