package com.ses.core.connpool;

import com.ses.core.connpool.connection.Conn;

public class CustomizedConnImpl extends Conn {
	public CustomizedConnImpl() {
		super();		
	}
	
	@Override
	public void createResource() {
	}

	@Override
	public void generate() {
	}
}
