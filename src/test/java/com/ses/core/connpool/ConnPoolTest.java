package com.ses.core.connpool;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.ses.app.CustomizedConnImpl;
import com.ses.common.loggerm.LogLevel;
import com.ses.common.loggerm.Logger;
import com.ses.core.connpool.ConnFactory;
import com.ses.core.connpool.ConnPool;
import com.ses.core.connpool.ConnPoolFactory;
import com.ses.core.connpool.connection.Conn;
import com.ses.core.connpool.exception.FailToCloseConnException;
import com.ses.core.connpool.exception.FailToCreateConnPoolException;
import com.ses.core.connpool.exception.FailToGetConnException;
import com.ses.core.connpool.exception.FailToReturnConnExceptipon;
import com.ses.core.connpool.queue.ConnPoolQueueInfo;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConnPoolTest {
	private static String TAG = ConnPoolTest.class.getSimpleName();
	private ConnFactory<CustomizedConnImpl> cf;
	private ConnPool cp;

	@BeforeClass
	public static void setUp() {
		Logger.init(LogLevel.DEBUG);
	}

	@Before
	public void setUpEach() {
		cf = new ConnFactory<>(CustomizedConnImpl.class);
		try {
			cp = ConnPoolFactory.getConnPool(cf);
		} catch (FailToCreateConnPoolException e) {
			Logger.error(TAG, e);
		}
	}

	@Test
	public void test1GetConn() {	

		try {
			cp.getConn();
		} catch (FailToGetConnException e) {
			Logger.error(TAG, e);
		}
		ConnPoolQueueInfo info = cp.getConnPoolInfo();
		info.showConnPoolStatus();
		assertEquals(info.getBorrowedCnt(), 1);
	}

	@Test
	public void test3ReturnConn() {
		
		Conn con = null;
		try {
			con = cp.getConn();
		} catch (FailToGetConnException e) {
			Logger.error(TAG, e);
		}
		ConnPoolQueueInfo info = cp.getConnPoolInfo();
		info.showConnPoolStatus();
		assertEquals(info.getBorrowedCnt(), 1);

		try {
			cp.returnConn(con);
		} catch (FailToReturnConnExceptipon e) {
			Logger.error(TAG, e);
		}
		info = cp.getConnPoolInfo();
		info.showConnPoolStatus();
		assertEquals(info.getBorrowedCnt(), 0);
	}

	@Test
	public void test4ConnClose() {
		
		Conn con = null;
		try {
			con = cp.getConn();
		} catch (FailToGetConnException e) {
			Logger.error(TAG, e);
		}
		ConnPoolQueueInfo info = cp.getConnPoolInfo();
		info.showConnPoolStatus();		

		try {
			con.close();
		} catch (FailToCloseConnException e) {
			Logger.error(TAG, e);
		}
		info = cp.getConnPoolInfo();
		info.showConnPoolStatus();
		assertEquals(info.getBorrowedCnt(), 0);
	}

	@Test
	public void test5Stop() {		
		cp.stop();
	}

	@Test
	public void test2RetryGet() {				
		ExecutorService executor = Executors.newCachedThreadPool();	
		for (int i = 0; i < 5; i++) {
			executor.execute(new Runnable() {
				@Override
				public void run() {
					Logger.info(TAG, "run.." + this.hashCode());
					try {
						cp.getConn();
					} catch (FailToGetConnException e) {
						Logger.error(TAG, e);
					}
					ConnPoolQueueInfo info = cp.getConnPoolInfo();
					info.showConnPoolStatus();
				}			
			});		
		}
		
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
