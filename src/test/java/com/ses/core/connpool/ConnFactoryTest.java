package com.ses.core.connpool;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ses.common.loggerm.LogLevel;
import com.ses.common.loggerm.Logger;
import com.ses.core.connpool.ConnFactory;
import com.ses.core.connpool.CustomizedConnImpl;

public class ConnFactoryTest {
	private static String TAG = ConnFactoryTest.class.getSimpleName();
	
	@BeforeClass
	public static void setUp() {
		Logger.init(LogLevel.DEBUG);
	}
	
	@Test
	public void testGetInstance() {
		ConnFactory<CustomizedConnImpl> cf = new ConnFactory<>(CustomizedConnImpl.class);
		for (int i = 0 ; i < 10 ; i++) {
			Logger.debug(TAG, "TEST " + i + " : " + cf.getInstatance().hashCode());			
		}
	}
}

