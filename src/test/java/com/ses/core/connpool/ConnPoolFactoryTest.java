package com.ses.core.connpool;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ses.common.loggerm.LogLevel;
import com.ses.common.loggerm.Logger;
import com.ses.core.connpool.ConnFactory;
import com.ses.core.connpool.ConnPoolFactory;
import com.ses.core.connpool.CustomizedConnImpl;
import com.ses.core.connpool.exception.FailToCreateConnPoolException;

public class ConnPoolFactoryTest {
	private static String TAG = ConnPoolFactoryTest.class.getSimpleName();

	@BeforeClass
	public static void setUp() {
		Logger.init(LogLevel.DEBUG);
	}
	
	@Test
	public void testGetInstance() {
		ConnFactory<CustomizedConnImpl> cf = new ConnFactory<>(CustomizedConnImpl.class);
		for (int i = 0 ; i < 10 ; i++) {
			try {
				Logger.debug(TAG, "TEST " + i + " : " + ConnPoolFactory.getConnPool(cf).hashCode());
			} catch (FailToCreateConnPoolException e) {
				Logger.error(TAG, e);
			}			
		}
	}
}
