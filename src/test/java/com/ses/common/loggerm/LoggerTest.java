package com.ses.common.loggerm;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ses.common.loggerm.LogLevel;
import com.ses.common.loggerm.Logger;

public class LoggerTest {

	private static String TAG = LoggerTest.class.getSimpleName();

	@BeforeClass
	public static void setUp() throws Exception {
		Logger.init(LogLevel.DEBUG);
	}

	@Test
	public void testDebug() throws Exception {
		for (int i = 0; i < 100; i++) {
			Logger.debug(TAG, "test: " + i);
		}
	}

	@Test
	public void testEnable() throws Exception {
		Logger.enable(true);
		Logger.debug(TAG, "testEnable before ");

		Logger.enable(false);
		Logger.debug(TAG, "testEnable after ");
	}

	@Test
	public void testEnableConsole() throws Exception {
		Logger.enableConsole(true);
		Logger.debug(TAG, "testEnableConsole before");

		Logger.enableConsole(false);
		Logger.debug(TAG, "testEnableConsole after");
	}

	@Test
	public void testError() throws Exception {
		try {
			throw new Exception();
		} catch (Exception e) {
			Logger.error(TAG, e);
		}
	}
	
	@Test
	public void testInfo() throws Exception {
		Logger.info(TAG, "info test");
	}
	
	@Test
	public void testWarn() throws Exception {
		Logger.info(TAG, "warn test");
	}
	
	@Test
	public void testSetLevel() throws Exception {
		Logger.debug(TAG, "set level test before");
		Logger.setLevel(LogLevel.INFO);
		Logger.debug(TAG, "set level test after");
	}
}
