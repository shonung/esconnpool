package com.ses.common.configm;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ses.common.configm.ConfigManagerFactory;
import com.ses.common.configm.IConfigManager;
import com.ses.common.configm.model.JsonSettingDefine;
import com.ses.common.loggerm.LogLevel;
import com.ses.common.loggerm.Logger;

public class ConfigManagerTest {
	private static String TAG = ConfigManagerTest.class.getSimpleName();
	private static IConfigManager cm = ConfigManagerFactory.JSON.getConfigManager();
	
	@BeforeClass
	public static void setUp() throws Exception {
		Logger.init(LogLevel.DEBUG);
	}
	
	@Test
	public void testGet() {
		Logger.debug(TAG, cm.get(JsonSettingDefine.POOL_SIZE.getDefine()));
	}	
}
